package pl.zmarzlak.labirynt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

public class StartActivity extends AppCompatActivity{
    private Button BtnMove1;
    private Button BtnMove2;
    private Button BtnMove3;
    private Button BtnMove4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        BtnMove1 = findViewById(R.id.button4);
        BtnMove2 = findViewById(R.id.button3);
        BtnMove3 = findViewById(R.id.button);
        BtnMove4 = findViewById(R.id.button2);
        BtnMove1.setOnClickListener(v -> openFirstLevel());
        BtnMove2.setOnClickListener(v -> openAboutProgram());
        BtnMove4.setOnClickListener(v -> continuePlaying());
        BtnMove3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(StartActivity.this, BtnMove3);
                menu.getMenuInflater().inflate(R.menu.levelmenu, menu.getMenu());
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        loadSelectedLevel(Integer.parseInt(item.getTitle().toString()));
                        return true;
                    }
                });
                menu.show();
            }
        });
    }
    void openFirstLevel() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("FIRST", true);
        startActivity(intent);
    }

    void loadSelectedLevel(int level) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("SELECTED", true);
        intent.putExtra("LVL", level - 1);
        startActivity(intent);
    }

    void continuePlaying() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    void openAboutProgram() {
        Intent intent = new Intent(this, AboutProgram.class);
        startActivity(intent);
    }
}

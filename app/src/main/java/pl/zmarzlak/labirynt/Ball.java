package pl.zmarzlak.labirynt;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class Ball extends View {
    private Paint red;
    private int size;
    private float posX;
    private float posY;
    private Rect circle;  // logika zrobiono na kwadracie a rysujemy kolo.
    private float speedX = 0, speedY = 0;

    public Ball(Context context, AttributeSet attrs, int size) {
        super(context, attrs);
        red = new Paint(Paint.ANTI_ALIAS_FLAG);
        red.setColor(Color.RED);
        this.size = size;
        circle = new Rect(0,0,0,0);
    }

    public void drawBall(Canvas canvas){
        canvas.drawCircle(posX, posY, size, red);
    }

    public void setPosition(float x, float y){
        circle.left= (int) (x - size);
        circle.top= (int) (y - size);
        circle.right= (int) (x + size);
        circle.bottom= (int) (y + size);
        posX = x;
        posY = y;
    }

    public void setSpeed(int x, int y){
        speedX = x;
        speedY = y;
    }

    public void updatePosition(float ax, float ay){
        float refreshTime = 0.01f;
        speedX = (ax * refreshTime);    // v = a*t
        speedY = (ay * refreshTime);

        float xS = speedX  / refreshTime;
        float yS = speedY / refreshTime;
        setPosition(posX - xS,posY - yS);
    }

    public Rect getCircle() {
        return circle;
    }
}

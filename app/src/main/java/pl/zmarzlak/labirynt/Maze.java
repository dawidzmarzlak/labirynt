package pl.zmarzlak.labirynt;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Toast;

public class Maze extends View {
    private final int ballSize = 30;
    private Ball ball;
    private ObstaclesManager obstacles;
    private static final int LOSE_GAME = -1;
    private static final int NEW_GAME = 0;
    private static final int STARTED_GAME = 1;
    private static final int WIN_GAME = 2;
    private int gameState = NEW_GAME;
    private int currentLevel;
    private SharedPreferences pref;

    public Maze(Context context, AttributeSet attrs) {
        super(context, attrs);
        ball = new Ball(context, attrs, ballSize);
        obstacles = new ObstaclesManager(context, attrs);
        pref = context.getSharedPreferences("currLvl", 0);
        currentLevel = pref.getInt("currLvl", 0);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setObtacles();
        obstacles.drawObtacles(canvas, getWidth(), gameState);
        ball.drawBall(canvas);
    }

    public void setObtacles(){
        if (gameState == NEW_GAME){
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((Activity) getContext()).getWindowManager()
                    .getDefaultDisplay()
                    .getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            obstacles.makeGame(ball, currentLevel, width, height, ballSize);
            gameState = STARTED_GAME;
        }
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public void restartGame() {
        if (gameState == WIN_GAME) {
            if (currentLevel < 8) {
                currentLevel++;
            }
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt("currLvl", currentLevel);
            editor.commit();
        }
        gameState = NEW_GAME;
    }

    public void update(float ax, float ay) {
        if (obstacles.collision()) {
            ball.setSpeed(0, 0);
            System.out.println("you Lose!");
            if (gameState == STARTED_GAME) {
                Toast toast = Toast. makeText(getContext(),"You lose, tap to restart",Toast. LENGTH_LONG);
                toast.show();
                gameState = LOSE_GAME;
            }
        } else if (obstacles.isWin()) {
            if (gameState == STARTED_GAME) {
                System.out.println("win");
                gameState = WIN_GAME;
                ball.setSpeed(0, 0);
                Toast toast = Toast. makeText(getContext(),"You win", Toast.LENGTH_LONG);
                toast.show();
            }
        } else {
            ball.updatePosition(ax, ay);
        }
    }
}

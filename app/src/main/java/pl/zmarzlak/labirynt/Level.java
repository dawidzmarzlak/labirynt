package pl.zmarzlak.labirynt;

import android.graphics.Rect;

import java.util.ArrayList;

public class Level {
    private Rect[] frame;
    private Rect[] obtacles;
    private Rect meta;
    private int ballX;
    private int ballY;

    public Level(Rect[] frame, Rect[] obtacles, Rect meta, int ballX, int ballY) {
        this.frame = frame;
        this.obtacles = obtacles;
        this.meta = meta;
        this.ballX = ballX;
        this.ballY = ballY;
    }

    public Rect[] getFrame() {
        return frame;
    }

    public Rect[] getObtacles() {
        return obtacles;
    }

    public Rect getMeta() {
        return meta;
    }

    public int[] getBallPosition() {
        return new int[] {ballX, ballY};
    }
}

package pl.zmarzlak.labirynt;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;
public class ObstaclesManager extends View {
    private Paint black, white, green;
    private Ball ball;
    private final int frameWidth = 20, lengthFromBorder = 0;
    private LinkedList<Level> levels = new LinkedList<>();
    private Level level;
    private int currentLevel;
    public ObstaclesManager(Context context, AttributeSet attrs) {
        super(context, attrs);
        white = new Paint(Paint.ANTI_ALIAS_FLAG);
        white.setColor(Color.WHITE);
        black = new Paint(Paint.ANTI_ALIAS_FLAG);
        black.setColor(Color.BLACK);
        green = new Paint(Paint.ANTI_ALIAS_FLAG);
        green.setColor(Color.GREEN);
    }
    public void drawObtacles(final Canvas canvas, int width, int gameState){
        canvas.drawRect(level.getFrame()[0], black);
        canvas.drawRect(level.getFrame()[1], white);
        canvas.drawRect(level.getMeta(), green);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(40);
        String textMsg;
        switch (gameState) {
            case -1:
                textMsg = "You lose, tap to restart";
                break;
            case 0:
                textMsg = "Tap to start";
                break;
            case 2:
                textMsg = "Tap to load next level";
                break;
            default:
                textMsg = "";
                break;
        }
        canvas.drawText(textMsg, width / 2 - 80, 100, paint);
        Arrays.asList(level.getObtacles()).forEach(obtacle -> canvas.drawRect(obtacle, black));
    }
    public boolean collision() {
        if (!Objects.isNull(level)) {
            for (Rect obtacle : level.getObtacles()) {
                if (!Objects.isNull(ball)) {
                    if (obtacle.intersect(ball.getCircle())) {
                        return true;
                    }
                }
            }
        }
        if (!Objects.isNull(ball) && !Objects.isNull(level.getFrame()[1])) {
            return !level.getFrame()[1].contains(ball.getCircle());
        }
        return false;
    }
    public boolean isWin() {
        if (!Objects.isNull(ball)) {
            return level.getMeta().contains(ball.getCircle());
        }
        return false;
    }

    public void makeGame(Ball ball, int currentLevel, int width, int height, int ballSize){
        this.ball = ball;
        this.currentLevel = currentLevel;
        levels.clear();
        levels.push(new Level(new Rect[] {
                new Rect(lengthFromBorder, lengthFromBorder, width - lengthFromBorder, height - lengthFromBorder),
                new Rect(lengthFromBorder + frameWidth, lengthFromBorder + frameWidth, width - lengthFromBorder - frameWidth, height - lengthFromBorder - frameWidth)
        }, new Rect[] {
                new Rect(width / 6,height / 10,width / 6 + frameWidth,height - lengthFromBorder - frameWidth),
                new Rect(width / 3, lengthFromBorder ,width / 3 + frameWidth,height - 10 * frameWidth),
                new Rect(width / 4 + 14 * frameWidth,width / 6 + frameWidth,width / 4 + 13 *frameWidth,height - lengthFromBorder - frameWidth),
                new Rect(width / 2 + 10 * frameWidth, lengthFromBorder ,width / 2 + frameWidth + 10 * frameWidth,height - 10 * frameWidth),
        }, new Rect(800, 100, 1000, 300), width / 10, height - 8 * frameWidth ));

        levels.push(new Level(new Rect[] {
                new Rect(lengthFromBorder, lengthFromBorder, width - lengthFromBorder, height - lengthFromBorder),
                new Rect(lengthFromBorder + frameWidth, lengthFromBorder + frameWidth, width - lengthFromBorder - frameWidth, height - lengthFromBorder - frameWidth)
        }, new Rect[] {
                new Rect(width / 5,height / 10,width / 5 + frameWidth,height - lengthFromBorder - frameWidth),
                new Rect(width / 2, lengthFromBorder ,width / 2 + frameWidth,height - 10 * frameWidth),
                new Rect(width / 2 + 14 * frameWidth,height / 5,width / 2 + 13 *frameWidth,height - lengthFromBorder - frameWidth),
        }, new Rect(800, 100, 1000, 300), width / 10, height - 8 * frameWidth ));

        levels.push(new Level(new Rect[] {
                new Rect(lengthFromBorder, lengthFromBorder, width - lengthFromBorder, height - lengthFromBorder),
                new Rect(lengthFromBorder + frameWidth, lengthFromBorder + frameWidth, width - lengthFromBorder - frameWidth, height - lengthFromBorder - frameWidth)
        }, new Rect[] {
                new Rect(width / 4,height / 10,width / 4 + frameWidth,height - lengthFromBorder - frameWidth),
                new Rect(width / 2, lengthFromBorder ,width / 2 + frameWidth,height - 10 * frameWidth),
        }, new Rect(800, 100, 1000, 300), width / 10, height - 8 * frameWidth ));

        levels.push(new Level(new Rect[] {
                new Rect(lengthFromBorder, lengthFromBorder, width - lengthFromBorder, height - lengthFromBorder),
                new Rect(lengthFromBorder + frameWidth, lengthFromBorder + frameWidth, width - lengthFromBorder - frameWidth, height - lengthFromBorder - frameWidth)
        }, new Rect[] {
                new Rect(lengthFromBorder + frameWidth, height - height / 3, width / 2 + 20 *frameWidth , height - height / 3 + frameWidth),
                new Rect(width / 8,height / 2,width - lengthFromBorder - frameWidth,height / 2 + frameWidth),
                new Rect(lengthFromBorder + frameWidth,height / 3,width / 2 + 20 *frameWidth,height / 3 + frameWidth),
                new Rect(width / 8,height / 6,width - lengthFromBorder - frameWidth, height / 6 + frameWidth),
        }, new Rect(800, 100, 1000, 300), width / 10, height - 8 * frameWidth ));

        levels.push(new Level(new Rect[] {
                new Rect(lengthFromBorder, lengthFromBorder, width - lengthFromBorder, height - lengthFromBorder),
                new Rect(lengthFromBorder + frameWidth, lengthFromBorder + frameWidth, width - lengthFromBorder - frameWidth, height - lengthFromBorder - frameWidth)
        }, new Rect[] {
                new Rect(width / 8,height / 2,width - lengthFromBorder - frameWidth,height / 2 + frameWidth),
                new Rect(lengthFromBorder + frameWidth,height / 3,width / 2 + 20 *frameWidth,height / 3 + frameWidth),
                new Rect(lengthFromBorder + frameWidth, height - height / 3, width / 2 + 20 *frameWidth , height - height / 3 + frameWidth),
        }, new Rect(100, 100, 300, 300), width / 10, height - 8 * frameWidth ));

        levels.push(new Level(new Rect[] {
                new Rect(lengthFromBorder, lengthFromBorder, width - lengthFromBorder, height - lengthFromBorder),
                new Rect(lengthFromBorder + frameWidth, lengthFromBorder + frameWidth, width - lengthFromBorder - frameWidth, height - lengthFromBorder - frameWidth)
        }, new Rect[] {
                new Rect(width / 2,height - height / 4,width / 2 + frameWidth,height - lengthFromBorder - frameWidth),
                new Rect(lengthFromBorder + frameWidth, height - height / 3, width / 2 + 15 *frameWidth, height - height / 3 + frameWidth),
                new Rect(width / 5,height / 3,width - lengthFromBorder - frameWidth,height / 3 + frameWidth),
        }, new Rect(700, 200, 900, 400), width / 10, height - 8 * frameWidth ));

        levels.push(new Level(new Rect[] {
                new Rect(lengthFromBorder, lengthFromBorder, width - lengthFromBorder, height - lengthFromBorder),
                new Rect(lengthFromBorder + frameWidth, lengthFromBorder + frameWidth, width - lengthFromBorder - frameWidth, height - lengthFromBorder - frameWidth)
        }, new Rect[] {
                new Rect(width / 2,height / 2,width / 2 + frameWidth,height - lengthFromBorder - frameWidth),
                new Rect(lengthFromBorder + frameWidth, height - height / 2, width / 3, height - height / 2 + frameWidth),
                new Rect(width / 5,height / 3,width - lengthFromBorder - frameWidth,height / 3 + frameWidth),
        }, new Rect(700, 200, 900, 400), width / 10, height - 8 * frameWidth ));
        levels.push(new Level(new Rect[] {
                new Rect(lengthFromBorder, lengthFromBorder, width - lengthFromBorder, height - lengthFromBorder),
                new Rect(lengthFromBorder + frameWidth, lengthFromBorder + frameWidth, width - lengthFromBorder - frameWidth, height - lengthFromBorder - frameWidth)
        }, new Rect[] {
                new Rect(width / 2,height / 3,width / 2 + frameWidth,height - lengthFromBorder - frameWidth)
        }, new Rect(700, 1700, 900, 1900), width / 10, height - 8 * frameWidth ));
        levels.push(new Level(new Rect[] {
                new Rect(lengthFromBorder, lengthFromBorder, width - lengthFromBorder, height - lengthFromBorder),
                new Rect(lengthFromBorder + frameWidth, lengthFromBorder + frameWidth, width - lengthFromBorder - frameWidth, height - lengthFromBorder - frameWidth)
        }, new Rect[] {
        }, new Rect(100, 100, 300, 300), 100 + frameWidth + 2 * ballSize, 900));
        level = levels.get(currentLevel);
        int[] ballPosition = levels.get(currentLevel).getBallPosition();
        ball.setPosition(ballPosition[0], ballPosition[1]);
    }
}
